Part II - Dimensionality reduction algorithms [READ ME]:

This folder contains the pratical work regarding the second part of the student's dissertation.

It contains the processing of the mice and TCGA data input, the tables that will be input to the script where it will be run the dimensionlity reduction algorithms,
the file with the the methods applied, and 3 folders with the output corresponding to the application of the methods:

-> [file] processInputCancerAnalysesFinalML.py: Receiving as input both the case study mice data and the TCGA data, it processes both of the different input types and
finds the genes that are commons to both the mice and the TCGA data. 

-> [folder] InputTablesFilesML: processInputCancerAnalysesFinalML.py outputs the tables with the processed data that will be input to the script
where the dimensionality reduction algorithms will be applied.

-> [file] cancerAnalysesFinalML.py: Different dimensionality reduction techniques are applied to the data to further explore if the mice data are different
from each other considering other features, in this case, the genes affected by SNVs and CNAs.

-> [folder] miceSamplesPlotsOutput, patientsTCGAPlotsOutput, micePatientsTCGASamplesPlotsOutput: these 3 folders have the resulting plots of applying
the dimensionality reduction algorithms to the data, first to the mice data only, then just with the TCGA patients data, and finally with them together.







