Input data [READ ME]:

This folder comprises the mice input data used for part I and part II of the thesis practical work and
also the TCGA pacients data with 33 different cancer types that complemented the mice data for part II

Mice:

-> [folder] CNVS: 
	-> [folder] excel_format_germline: the CNAs of the samples of each mouse data converted into a csv file, including the
					   chromosome it affects, starting and end position, baf, total copy number,
					   major and minor copy number, among others.

-> [folder] variantes_somaticas: 
	-> [folder] anotacoes: the genes affected by SNVs in each sample of the mice.


	-> [folder] excel_format: the SNVs data, including the chromosome it affects, its position, 
				  the reference and alternative nucleotide bases, the filter value,
				  the reference and normal read counts, among others.



TCGA: 

-> [file] e100_biomart_gene_positions.tab: each gene starting and end positions, code and name.


-> [folder] CNV:
	-> [file] TCGA-cancer_type_Masked_Copy_Number_Segment.csv: corresponding to the CNVs information of
								   the samples of each patient, excluding the copy numbers.

	-> [folder] excel_format: focal scores: the total copy number of each gene in each CNV sample.


-> [folder] SNV:

        -> [file] TCGA.cancer_type.mutect_somatic.csv: The reduced files of TCGA genes SNVs information for the samples of the patients.
	
	-> [file] reduceSizeFilesTCGA.py: reduce the originAL TCGA genes SNVs files that had many uncessary attributes and occupied  many GBs.

	