# -*- coding: utf-8 -*-
"""
Created on Fri Jul 30 18:46:20 2021

@author: david
"""


# To open system files
import os

# Pandas also for data management
import pandas as pd


col_list = ['Hugo_Symbol', 'Chromosome', 'Start_Position', 'Reference_Allele', 'Tumor_Seq_Allele2', \
            'Tumor_Sample_Barcode', 't_ref_count', 't_alt_count', 'n_depth', 'Consequence', 'FILTER']

    
# sort filenames
for filename in sorted(os.listdir(os.getcwd())):
    if filename.endswith('.csv'):
        with open(os.path.join(os.getcwd(), filename), 'r'):
            # because of the dtype warning https://stackoverflow.com/questions/24251219/pandas-read-csv-low-memory-and-dtype-options
            data = pd.read_csv(filename, delimiter=';', dtype = 'unicode', usecols = col_list)

            data.to_csv(filename, index = False, encoding='utf-8', sep = ';')